<?php
/**
 * @copyright Copyright (c) 2018 Julius Härtl <jus@bitgrid.net>
 *
 * @author Julius Härtl <jus@bitgrid.net>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @var array $_
 * @var \OCP\IL10N $l
 * @var \OCP\Defaults $theme
 */
?>

<div class="page" data-title="Bienvenue !" data-subtitle=""
          style="
    height:100%;
    width:100%;
    background-image: url('<?php p(image_path('firstrunwizard', 'background-cloud.png')); ?>');
    font-size:1em;
    " >
	<div class="content content-values">
          <h2>Fichiers</h2>

          <div style="text-align:center">
    <img src="<?php p(image_path('firstrunwizard', 'fichiers1.svg')); ?>" style="width:70%"/>
    </div>

          <ul style="line-height: 8px;">
          <li>1. Créer un nouveau document/dossier (icone +)</li>
          <li>2. Liste des fichiers du dossier en cours</li>
          <li>3. Actions disponibles pour un fichier</li>
          <li>4. Basculer vue liste/mosaïque</li>          
          </li>
</div>


       
